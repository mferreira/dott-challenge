import styled from '@emotion/styled'

export const Wrapper = styled.div`
  position: relative;
  margin: 0;
`

export const ButtonWrap = styled.div`
  position: absolute;
  z-index: 1000;
  bottom: 0;
  height: auto;
  width: 100%;
`
