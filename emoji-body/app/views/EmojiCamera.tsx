import { useState, useEffect } from 'react'

import {
  Button,
  Video,
  BodyPart,
  StatusMessage,
} from '../components'
import { getCameraIds, hasMediaSupport, initPoseDetection, parsePoses } from '../utils'
import { CurrentCameraType, EmojiObject, ErrorType } from '../types'
import { Wrapper, ButtonWrap } from './EmojiCamera.styled'

const EmojiCamera: React.FC = () => {
  const [posesReady, setPosesReady] = useState(false)
  const [cameras, setCameras] = useState<CurrentCameraType[]>([])
  const [currentCamera, setCurrentCamera] = useState<number | undefined>(undefined)
  const [emojiObjects, setEmojiObjects] = useState<EmojiObject[]>([])
  const [error, setError] = useState<ErrorType>(null)

  async function initCameraDevices () {
    const list = await getCameraIds()
    setCameras(list)
    setCurrentCamera(0)
  }

  useEffect(() => {
    if (!hasMediaSupport()) {
      setError('No support for video capture')
      return
    }

    initCameraDevices()
  }, [])

  function selectNextCamera() {
    if (currentCamera !== undefined && cameras.length > 1) {
      const nextCamera = currentCamera + 1
      setCurrentCamera(nextCamera !== cameras.length ? nextCamera : 0)
    }
  }

  const handleVideoReady = (video: HTMLVideoElement) => {
    initPoseDetection({
      video,
      onPosesReady: poses => {
        if (!poses.length) {
          setPosesReady(false)
        }
        else {
          setPosesReady(true)

          // process all poses (1 per person)
          setEmojiObjects(parsePoses(poses))
        }
      }
    })
  }

  if (error) {
    return (
      <p>{error}</p>
    )
  }

  const camera = currentCamera !== undefined ? cameras[currentCamera] : null

  return (
    <Wrapper>
      {camera && (
        <Video
          camera={camera}
          onVideoReady={handleVideoReady}
        />
      )}

      {posesReady && emojiObjects.length > 0 && emojiObjects.map((emoji: EmojiObject, index: number) => (
        <BodyPart {...emoji.position} key={index+emoji.key}>
          {emoji.char}
        </BodyPart>
      ))}

      <ButtonWrap>
        <Button onClick={selectNextCamera}>Next Camera</Button>
        {!posesReady && (
          <StatusMessage>
            Looking for human beings ... 👀
          </StatusMessage>
        )}
      </ButtonWrap>
    </Wrapper>
  )
}

export default EmojiCamera
