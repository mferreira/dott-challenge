import { isMobile } from './utils';

export const config = {
  input: {
    imageScaleFactor: isMobile() ? 0.3 : 0.5,
    netArchitecture: isMobile() ? 0.5 : 0.75,
    outputStride: 16,
  },
  multiPoseDetection: {
    maxPoseDetections: 5,
    minPartConfidence: 0.1,
    minPoseConfidence: 0.15,
    nmsRadius: 30.0
  }
};
