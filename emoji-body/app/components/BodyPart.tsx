import styled from '@emotion/styled'
import { Position } from '../types'

const BodyPart = styled.span<Position>`
  position: absolute;
  left: ${p => p.x}px;
  top: ${p => p.y}px;
`

export default BodyPart
