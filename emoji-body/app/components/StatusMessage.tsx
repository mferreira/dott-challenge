import styled from '@emotion/styled'

const StatusMessageBase: React.FC = ({children, ...rest}) => (
  <p {...rest}>
    {children}
  </p>
)

const StatusMessage = styled(StatusMessageBase)`
  width: 100%;
  background-color: black;
  color: white;
  position: absolute;
`

export default StatusMessage
