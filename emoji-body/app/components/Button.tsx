import styled from '@emotion/styled'

const Button = styled.button`
  height: 44px;
  z-index: 100;
`

export default Button
