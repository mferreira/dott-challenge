import { useEffect, useRef, useState } from 'react'

import { debouncer, getCameraStream } from '../utils'
import { CurrentCameraType, Size } from '../types'

const VIDEO_WIDTH = 1280
const VIDEO_HEIGHT = 720

export interface VideoProps {
  camera: CurrentCameraType
  onVideoReady(video: HTMLVideoElement): void
}

const Video: React.FC<VideoProps> = ({ camera, onVideoReady }) => {
  let videoRef = useRef<HTMLVideoElement>(null)

  const [currentCamera, setCurrentCamera] = useState<string | undefined>(undefined)
  const [videoSize, setVideoSize] = useState<Size>({width: VIDEO_WIDTH, height: VIDEO_HEIGHT})

  const adjustVideoSize = debouncer(function() {
    // ratio between window and video width
    let ratio = window.innerWidth / VIDEO_WIDTH
    // get the window width
    let width = window.innerWidth

    let height

    if (window.screen.orientation.type.split('-')[0] === 'portrait') {
      height = window.innerHeight
    }
    else {
      // get proportional height with ratio
      height = videoSize.height * ratio

      // adjust if video won't fit in height
      if (height > window.innerHeight) {
        height = window.innerHeight
        ratio = height / VIDEO_HEIGHT
        width = videoSize.width * ratio
      }
    }

    // set video size state
    setVideoSize({width, height})
  }, 100)

  async function resetCameraStream() {
    if (camera && videoRef.current) {
      const stream = await getCameraStream({
        currentCamera: camera,
        videoSize
      })

      videoRef.current.srcObject = stream
      videoRef.current.onloadedmetadata = () => {
        // start streaming video when video metadata is ready
        videoRef.current!.play()
        onVideoReady(videoRef.current!)
      }
      videoRef.current.load()

    }
  }

  useEffect(() => {
    // adjust the video size to the window for the first time
    adjustVideoSize()
    // re-adjust if the window is resized or rotated
    window.addEventListener('resize', adjustVideoSize)
    window.addEventListener('orientationchange', adjustVideoSize)
    // stop re-adjusting when component is unmounted
    return () => {
      window.removeEventListener('resize', adjustVideoSize)
      window.removeEventListener('orientationchange', adjustVideoSize)
    }
  }, [])

  useEffect(() => {
    if (camera && camera !== currentCamera) {
      setCurrentCamera(camera)
      resetCameraStream()
    }
  }, [camera])

  const isCameraReady = () => camera !== null

  return isCameraReady()
    ? (
        <video playsInline={true} ref={videoRef} {...videoSize} />
    ) : (
      <p>Camera is not ready yet</p>
    )
}

export default Video
