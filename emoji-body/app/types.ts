export type CurrentCameraType = string | undefined

export type ErrorType = string | null

export interface Position {
  x: number
  y: number
}

export interface Size {
  width: number
  height: number
}

export interface CameraConfig {
  currentCamera: CurrentCameraType
  videoSize: Size
}

export interface EmojiObject {
  char: string
  position: Position
  key: string
}
