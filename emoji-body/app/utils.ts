import * as posenet from '@tensorflow-models/posenet'
import { CameraConfig, EmojiObject} from './types'

export const isMobile = (): boolean =>
  /Android/i.test(navigator.userAgent) || /iPhone|iPad|iPod/i.test(navigator.userAgent)

export const hasMediaSupport = (): boolean => {
  if (!navigator.mediaDevices || !navigator.mediaDevices.getUserMedia || !navigator.mediaDevices.enumerateDevices) {
    return false
  }

  return true
}

export const debouncer = (f: () => void, wait: number, args?: any) => {
  let timeout: NodeJS.Timeout
  return () => {
    clearTimeout(timeout)
    timeout = setTimeout(f, wait, args)
  }
}

export const getCameraIds = async (): Promise<string[]> => {
  const list = await navigator.mediaDevices.enumerateDevices();

  return list
    .filter(({ kind }) => kind === "videoinput")
    .map(({ deviceId }) => deviceId)
}

export const getCameraStream = async ({ currentCamera, videoSize }: CameraConfig): Promise<MediaStream> => {
  const constraints = {
    audio: false,
    video: {
      deviceId: { exact: currentCamera },
      facingMode: 'user',
      ...videoSize
    }
  }

  return await navigator.mediaDevices.getUserMedia(constraints)
}

interface poseDetectionArgs {
  video: HTMLVideoElement
  onPosesReady(poses: posenet.Pose[]): void
}

export const initPoseDetection = async ({video, onPosesReady}: poseDetectionArgs) => {
  if (video) {
    const arch = isMobile() ? 0.5 : 0.75
    const net = await posenet.load(arch as posenet.MobileNetMultiplier)
    const estimatePoses = async function () {
      // console.log('Estimating poses ...')
      // onPosesReady([])
      const poses = await net.estimateMultiplePoses(video)
      onPosesReady(poses)
      requestAnimationFrame(() => estimatePoses())
    }

    await estimatePoses()
  }
}

export const parsePoses = (poses: posenet.Pose[]): EmojiObject[] => {
  const newEmojiNodes: EmojiObject[] = []
  poses.forEach((pose: posenet.Pose) => {
    for (const point of pose.keypoints) {
      switch (point.part) {
        case 'leftEye':
          newEmojiNodes.push({
            char: '🎾',
            position: point.position,
            key: point.part
          })
          continue;

        case 'rightEye':
          newEmojiNodes.push({
            char: '️️🎾',
            position: point.position,
            key: point.part
          })
          continue;

        case 'leftShoulder':
          newEmojiNodes.push({
            char: '️️😇️',
            position: point.position,
            key: point.part
          })
          continue;

        case 'rightShoulder':
          newEmojiNodes.push({
            char: '️️😈',
            position: point.position,
            key: point.part
          })
          continue;

        default:
          continue;
      }
    }
  })
  return newEmojiNodes
}
