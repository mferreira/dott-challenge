import { addN } from '../src/addN'

describe('addN', () => {
  const addEight = addN(8)

  it('should return a new function', () => {
    expect(addEight).toBeInstanceOf(Function)
  })

  it('should resolve to 15', () => {
    expect(addEight(7)).toBe(15)
  })

  it('should resolve in 108', () => {
    expect(addEight(100)).toBe(108)
  })
})
